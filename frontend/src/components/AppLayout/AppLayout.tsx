import React, { Component } from 'react';
import { connect } from 'react-redux';

import { MobileMenuTypes } from '../../store/actions';
import { State } from '../../store/initialState';
import { selectIsDataLoaded, selectIsDisconnect, selectMobileMenu } from '../../store/selectors';
import { Chat } from '../Chat/Chat';
import { Header } from '../Header/Header';
import { TradeInformation } from '../TradeInformation/TradeInformation';
import { Trades } from '../Trades/Trades';
import styles from './AppLayout.module.css';
import { DisconnectPopoup } from './DisconnectPopup/DisconnectPopup';

interface AppLayoutProps {
  isDataLoaded: boolean;
  isDisconnect: boolean;
  selectedMobileMenu: string;
}
const AppLayoutComponent = ({
  isDataLoaded,
  isDisconnect,
  selectedMobileMenu
}: AppLayoutProps) => {
  let menuClass = styles.showChat;
  switch (selectedMobileMenu) {
    case MobileMenuTypes.info:
      menuClass = styles.showInfo;
      break;
    case MobileMenuTypes.list:
      menuClass = styles.showList;
      break;
  }

  const layout = isDataLoaded ? (
    <div>
      <Header />
      <div className={styles.contentWraper}>
        <div className={`${styles.content} ${menuClass}`}>
          <Trades />
          <Chat />
          <TradeInformation />
        </div>
      </div>
      {isDisconnect && <DisconnectPopoup />}
    </div>
  ) : (
    <div>loading...</div>
  );
  return layout;
};

const mapProps = (state: State) => ({
  isDataLoaded: selectIsDataLoaded(state),
  isDisconnect: selectIsDisconnect(state),
  selectedMobileMenu: selectMobileMenu(state)
});

export const AppLayout = connect(mapProps)(AppLayoutComponent);
