import React, { Component } from 'react';

import styles from './DisconnectPopup.module.css';

export const DisconnectPopoup = () => (
  <div className={styles.disconnectbg}>
    <div className={styles.disconnect}>
      Disconnected
      <a href="."> reload </a>
      <a href="/">get random user</a>
    </div>
  </div>
);
