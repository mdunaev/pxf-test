import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { deleteTradeAction } from '../../../store/actions';
import { State, Trade, User } from '../../../store/initialState';
import { selectCurrentTrade, selectTraderUser } from '../../../store/selectors';
import styles from './ChatHeader.module.css';
import deleteIcon from './delete.svg';

interface ChatHeaderProps {
  deleteTrade: Function;
  trader: User;
  trade: Trade;
}
const ChatHeaderComponent = ({
  deleteTrade,
  trade,
  trader
}: ChatHeaderProps) => {
  const deleteClickHandler = () => deleteTrade(trade.tradeId);
  return (
    <div className={styles.header}>
      <div className={styles.left}>
        <div className={styles.payment}>{trade.paymentMethod}</div>
        <div className={styles.name}>{trader.name}</div>
        <div className={styles.rating}>
          <span className={styles.positive}>
            {trader.positiveReputation}
          </span>

           /
          <span className={styles.negative}>
             {trader.negativeReputation}
          </span>
        </div>
      </div>
      <div className={styles.right}>
        <div
          className={styles.deleteIcon}
          onClick={deleteClickHandler}
        >
          <img src={deleteIcon} />
        </div>
      </div>
    </div>
  );
};

const mapProps = (state: State) => ({
  trader: selectTraderUser(state),
  trade: selectCurrentTrade(state)
});

const mapDispatch = (dispatch: Dispatch) => ({
  deleteTrade: (tradeId: string) =>
    dispatch(deleteTradeAction(tradeId))
});

export const ChatHeader = connect(
  mapProps,
  mapDispatch
)(ChatHeaderComponent);
