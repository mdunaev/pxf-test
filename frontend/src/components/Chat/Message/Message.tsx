import { formatDistance } from 'date-fns';
import React from 'react';

import { Avatar } from '../../Avatar/Avatar';
import styles from './Message.module.css';

interface MessageProps {
  userName: string;
  userId: string;
  isUserMessage: boolean;
  date: string;
  text: string;
  isImage: boolean;
  tradeId: string;
}
export const Message = ({
  userName,
  userId,
  isUserMessage,
  date,
  text,
  isImage,
  tradeId
}: MessageProps) => {
  const content = isImage ? (
    <a href={text} target="_blank">
      <div
        className={styles.image}
        style={{ backgroundImage: `url(${text})` }}
      />
    </a>
  ) : (
    <div className={styles.text}>{text}</div>
  );
  return (
    <div
      className={`${styles.message} ${isUserMessage &&
        styles.usermessage}`}
    >
      <a className={styles.avatar} href={`/${userId}/${tradeId}/`}>
        <Avatar userId={userId} />
      </a>
      <div className={styles.right}>
        <div className={styles.content}>{content}</div>
        <div className={styles.date}>
          {formatDistance(new Date(date), new Date())} ago
        </div>
      </div>
    </div>
  );
};
