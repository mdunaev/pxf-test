import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { changeMobileMenuAction, MobileMenuTypes } from '../../../store/actions';
import { State } from '../../../store/initialState';
import { selectMobileMenu } from '../../../store/selectors';
import styles from './MobileMenu.module.css';

interface MobileMenuProps {
  selectedItem: string;
  changeMenu: Function;
}
const MobileMenuComponent = ({
  selectedItem,
  changeMenu
}: MobileMenuProps) => {
  const menuSelectHandler = (key: string) => () => changeMenu(key);

  const generateMenuItem = (key: any, i: number) => {
    const itemText = MobileMenuTypes[key];
    const isSelected = itemText === selectedItem;
    const classes = `${styles.item} ${isSelected && styles.selected}`;
    return (
      <div
        key={i}
        className={classes}
        onClick={menuSelectHandler(itemText)}
      >
        {MobileMenuTypes[key]}
      </div>
    );
  };

  return (
    <div className={styles.mobileMenu}>
      {Object.keys(MobileMenuTypes).map(generateMenuItem)}
      <div className={styles.item} />
    </div>
  );
};

const mapProps = (state: State) => ({
  selectedItem: selectMobileMenu(state)
});

const mapDispatch = (dispatch: Dispatch) => ({
  changeMenu: (menu: MobileMenuTypes) =>
    dispatch(changeMobileMenuAction(menu))
});

export const MobileMenu = connect(
  mapProps,
  mapDispatch
)(MobileMenuComponent);
