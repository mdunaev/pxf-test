import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { changeFitlerAction } from '../../../store/actions';
import { State } from '../../../store/initialState';
import { selectTradesFilter } from '../../../store/selectors';
import styles from './TradeFilter.module.css';

export enum TradesFilterTypes {
  last = 'By date',
  notSeen = 'Not seen',
  hasPaid = 'has paid'
}

interface TradeFilterProps {
  selectedFilter: TradesFilterTypes;
  changeFilter: Function;
}
const TradeFilterComponent = ({
  selectedFilter,
  changeFilter
}: TradeFilterProps) => {
  const changeFilterHandler = (filterName: string) => () =>
    changeFilter(filterName);
  return (
    <div className={styles.filter}>
      {Object.keys(TradesFilterTypes).map(
        (filterName: any, i: number) => {
          const classes =
            selectedFilter === filterName
              ? `${styles.filterType} ${styles.selected}`
              : styles.filterType;
          return (
            <div
              key={i}
              className={classes}
              onClick={changeFilterHandler(filterName)}
            >
              {TradesFilterTypes[filterName]}
            </div>
          );
        }
      )}
    </div>
  );
};

const mapProps = (state: State) => ({
  selectedFilter: selectTradesFilter(state)
});
const mapDispatch = (dispatch: Dispatch) => ({
  changeFilter: (newFilter: TradesFilterTypes) =>
    dispatch(changeFitlerAction(newFilter))
});
export const TradeFilter = connect(
  mapProps,
  mapDispatch
)(TradeFilterComponent);
