import './index.css';

import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, compose, createStore, Store } from 'redux';
import thunk from 'redux-thunk';

import App from './App';
import { State } from './store/initialState';
import { reducer } from './store/reducers';
import { transport } from './store/transport';

const history = createBrowserHistory();
const composeEnhancer =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middlewares = applyMiddleware(routerMiddleware(history), thunk);

const store: Store<State> = createStore(
  connectRouter(history)(reducer),
  composeEnhancer(middlewares)
);

transport(store);

ReactDOM.render(
  <Provider store={store}>
    <App history={history} />
  </Provider>,
  document.getElementById('root')
);
