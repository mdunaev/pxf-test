import { TradesFilterTypes } from '../components/Trades/TradeFilter/TradesFilter';
import { MobileMenuTypes } from './actions';

export const initialState: State = {
  serverState: {
    sellers: [],
    buyers: [],
    trades: [],
    id: ''
  },
  btcPrice: 0,
  selectedTradeId: '',
  chatMessage: '',
  isDisconnect: false,
  selectedFilter: 'last' as TradesFilterTypes,
  mobileMenu: MobileMenuTypes.list,
  isImageSending: false
};

export interface State {
  serverState: ServerState;
  btcPrice: number;
  selectedTradeId: string;
  chatMessage: string;
  isDisconnect: boolean;
  selectedFilter: TradesFilterTypes;
  mobileMenu: MobileMenuTypes;
  isImageSending: boolean;
}

export interface ServerState {
  sellers: User[];
  buyers: User[];
  trades: Trade[];
  id: string;
}

export interface Trade {
  tradeId: string;
  buyerId: string;
  sellerId: string;
  paymentMethod: string;
  amount: number;
  isPaid: boolean;
  chatMessages: ChatMessage[];
  hasNewMessages: boolean;
  date: string;
}

export interface ChatMessage {
  date: string;
  fromId: string;
  text: string;
  isImage: boolean;
}

export interface User {
  id: string;
  name: string;
  positiveReputation: number;
  negativeReputation: number;
}
