import { AnyAction } from 'redux';

import { TradesFilterTypes } from '../components/Trades/TradeFilter/TradesFilter';
import {
  CHANGE_MOBILE_MENU,
  CHANGE_TRADE,
  CHANGE_TRADES_FILTER,
  CHAT_INPUT_CHANGED,
  DISCONNECT,
  MobileMenuTypes,
  NEW_MESSAGE,
  REFRESH_STATE,
  SEND_IMAGE,
  SEND_MESSAGE,
  UPDATE_BTC_PRICE,
} from './actions';
import { initialState, State, Trade } from './initialState';
import { selectTradeById } from './selectors';

// TODO: Split to parts using combineReducers
export function reducer(
  state: State = initialState,
  action: AnyAction
): State {
  switch (action.type) {
    case REFRESH_STATE:
      return {
        ...state,
        serverState: action.newState,
        selectedTradeId: action.newTradeId,
        isDisconnect: false
      };
    // CHANGE_TRADE:
    // If selected trade is not seen
    // Change trades filter to 'By Date'
    // Open trade in chat view for mobile
    // Mark trade as readed - hasNewMessages: false
    case CHANGE_TRADE:
      const selectedFilter =
        TradesFilterTypes[state.selectedFilter as any];
      return {
        ...state,
        selectedTradeId: action.tradeId,
        selectedFilter:
          selectedFilter === TradesFilterTypes.notSeen
            ? ('last' as TradesFilterTypes)
            : (state.selectedFilter as TradesFilterTypes),
        mobileMenu: MobileMenuTypes.chat,
        serverState: {
          ...state.serverState,
          trades: state.serverState.trades.map((trade: Trade) => {
            if (trade.tradeId === action.tradeId) {
              return {
                ...trade,
                hasNewMessages: false
              };
            }
            return trade;
          })
        }
      };
    case UPDATE_BTC_PRICE:
      return {
        ...state,
        btcPrice: action.price
      };
    case SEND_IMAGE:
      return {
        ...state,
        isImageSending: true
      };
    case CHANGE_TRADES_FILTER:
      return {
        ...state,
        selectedFilter: action.newFilter
      };
    case CHANGE_MOBILE_MENU:
      return {
        ...state,
        mobileMenu: action.menu
      };
    case CHAT_INPUT_CHANGED:
      return {
        ...state,
        chatMessage: action.text
      };
    case SEND_MESSAGE:
      return {
        ...state,
        chatMessage: ''
      };
    case DISCONNECT:
      return {
        ...state,
        isDisconnect: true
      };
    // NEW_MESSAGE
    // Get trade for new message
    // Put message to trade
    // Remove old trade from array of trades
    // Put new trade to trades
    case NEW_MESSAGE:
      const trade = selectTradeById(state, action.message.tradeId);
      const updatedTrade: Trade = {
        ...trade,
        chatMessages: [
          {
            date: action.message.date,
            fromId: action.message.fromId,
            text: action.message.text,
            isImage: action.message.isImage
          },
          ...trade.chatMessages
        ],
        hasNewMessages:
          state.selectedTradeId !== action.message.tradeId
      };
      const filteredTrades = state.serverState.trades.filter(
        (trade: Trade) => trade.tradeId !== action.message.tradeId
      );
      return {
        ...state,
        isImageSending: false,
        serverState: {
          ...state.serverState,
          trades: [updatedTrade, ...filteredTrades]
        }
      };
    default:
      return state;
  }
}
