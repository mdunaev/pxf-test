import * as express from 'express';
import * as socketIO from 'socket.io';

import {
  addChatMessageToState,
  deleteTrade,
  generateInitialState,
  getStateForId,
  markTradeAsRead,
  payTrade,
  selectIsUserExist,
  selectRandomSellerId,
  selectTradeUsersById,
  Trade,
} from './stateController';

const app = express();
var http = require('http').Server(app);
var io = socketIO(http, {
  path: '/api/socket.io'
});

let sockets = {};
io.on('connection', function(socket) {
  let socketUserId = '';
  socket.on('get-user-state', ({ userId }) => {
    if (sockets[userId]) {
      sockets[userId].emit('disconnected', {
        status: 'disconnected'
      });
    }
    sockets[socketUserId] = null;
    if (selectIsUserExist(state, userId)) {
      sockets[userId] = socket;
      socket.emit('full-state', getStateForId(state, userId));
      socketUserId = userId;
    } else {
      let id = selectRandomSellerId(state);
      socket.emit('full-state', getStateForId(state, id));
      sockets[id] = socket;
      socketUserId = id;
    }
  });

  socket.on('read-trade', ({ tradeId }: any) => {
    state = markTradeAsRead(state, tradeId);
  });

  socket.on('delete-trade', ({ tradeId }: any) => {
    const users = selectTradeUsersById(state, tradeId);
    state = deleteTrade(state, tradeId);
    users.map(
      (userId: string) =>
        sockets[userId] &&
        sockets[userId].emit(
          'full-state',
          getStateForId(state, userId)
        )
    );
  });
  socket.on('pay-trade', ({ tradeId }: any) => {
    const users = selectTradeUsersById(state, tradeId);
    state = payTrade(state, tradeId);
    users.map(
      (userId: string) =>
        sockets[userId] &&
        sockets[userId].emit(
          'full-state',
          getStateForId(state, userId)
        )
    );
  });

  socket.on('chat-message', data => {
    state = addChatMessageToState(
      state,
      data.tradeId,
      data.userId,
      data.message,
      data.isImage
    );
    if (sockets[data.userId]) {
      const messageData = {
        ...data,
        date: new Date()
      };

      const { sellerId, buyerId }: ChatUsers = getUsersFromTradeId(
        data.tradeId
      );

      socket.emit('chat-message', messageData);

      const secondUserId =
        messageData.userId === sellerId ? buyerId : sellerId;
      if (sockets[secondUserId]) {
        sockets[secondUserId].emit('chat-message', messageData);
      }
    }
  });
});

http.listen(8070, function() {
  console.log('Socket server started');
});

let state = generateInitialState();

interface ChatUsers {
  sellerId: string;
  buyerId: string;
}

const getUsersFromTradeId = (tradeId: string): ChatUsers => {
  return state.trades.reduce(
    (acc: string, trade: Trade) => {
      if (trade.tradeId === tradeId) {
        return {
          sellerId: trade.sellerId,
          buyerId: trade.buyerId
        };
      }
      return acc;
    },
    { sellerId: '', buyerId: '' }
  ) as ChatUsers;
};
